package objects;

/**
 * Trida reprezentujici herni cil.
 *
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class GameGoal {
    private String name;
    private String description;

    /**
     * Konstruktor herniho cile.
     *
     * @param name Jmeno herniho cile
     * @param description Slovni popis herniho cile
     */
    public GameGoal(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * Metoda ke kontrole splneni herniho cile.
     *
     * @return Logicka hodnota, zda je cil splnen
     */
    public boolean fulfilled() {
        return false;
    }
}
