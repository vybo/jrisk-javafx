package objects;

/**
 * Herni karta reprezentujici teritorium, nikterak funkcne.
 *
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class TerritoryCard {
    private Territory representingTerritory;
    private int starValue;

    /**
     * Konstruktor tridy.
     *
     * @param representingTerritory Teritorium, ktere karta obrazne reprezentuje
     * @param starValue Hodnota, ktere karta je.
     */
    public TerritoryCard(Territory representingTerritory, int starValue) {
        this.representingTerritory = representingTerritory;
        this.starValue = starValue;
    }

    /**
     * Metoda vracejici hodnotu karty.
     *
     * @return Hodnota karty
     */
    public int getStarValue() {
        return this.starValue;
    }

    /**
     * Metoda vracejici nazev teritoria, ktere reprezentuje.
     *
     * @return Nazev reprezentovaneho teritoria
     */
    public String getName() {
        return this.representingTerritory.getName();
    }
}
