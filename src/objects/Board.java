package objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Trida hlavni hraci desky, obsahuje prakticky vse.
 *
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class Board {
    private ArrayList<Territory> territories;

    /**
     * Konstruktor tridy, ktery provede kompletni inicializaci hernich objektu.
     */
    public Board(){
        territories = new ArrayList<Territory>();

        final Territory alaska = new Territory(1, "Alaska", ContinentName.NORTH_AMERICA, null, null, null);
        final Territory alberta = new Territory(2, "Alberta", ContinentName.NORTH_AMERICA, null, null, null);
        final Territory centralAmerica = new Territory(3, "Central America", ContinentName.NORTH_AMERICA, null, null, null);
        final Territory easternUsa = new Territory(4, "Eastern USA", ContinentName.NORTH_AMERICA, null, null, null);
        final Territory greenland = new Territory(5, "Greenland", ContinentName.NORTH_AMERICA, null, null, null);
        final Territory northwestUsa = new Territory(6, "Northwest USA", ContinentName.NORTH_AMERICA, null, null, null);
        final Territory ontario = new Territory(7, "Ontario", ContinentName.NORTH_AMERICA, null, null, null);
        final Territory quebec = new Territory(8, "Quebec", ContinentName.NORTH_AMERICA, null, null, null);
        final Territory westernUsa = new Territory(9, "Western USA", ContinentName.NORTH_AMERICA, null, null, null);

        final Territory argentina = new Territory(10, "Argentina", ContinentName.SOUTH_AMERICA, null, null, null);
        final Territory brazil = new Territory(11, "Brazil", ContinentName.SOUTH_AMERICA, null, null, null);
        final Territory peru = new Territory(12, "Peru", ContinentName.SOUTH_AMERICA, null, null, null);
        final Territory venezuela = new Territory(13, "Venezuela", ContinentName.SOUTH_AMERICA, null, null, null);

        final Territory greatBritain = new Territory(14, "Great Britain", ContinentName.EUROPE, null, null, null);
        final Territory iceland = new Territory(15, "Iceland", ContinentName.EUROPE, null, null, null);
        final Territory northernEurope = new Territory(16, "Northern Europe", ContinentName.EUROPE, null, null, null);
        final Territory scandinavia = new Territory(17, "Scandinavia", ContinentName.EUROPE, null, null, null);
        final Territory southernEurope = new Territory(18, "Southern Europe", ContinentName.EUROPE, null, null, null);
        final Territory ukraine = new Territory(19, "Ukraine", ContinentName.EUROPE, null, null, null);
        final Territory westernEurope = new Territory(20, "Western Europe", ContinentName.EUROPE, null, null, null);

        final Territory congo = new Territory(21, "Congo", ContinentName.AFRICA, null, null, null);
        final Territory eastAfrica = new Territory(22, "East Africa", ContinentName.AFRICA, null, null, null);
        final Territory egypt = new Territory(23, "Egypt", ContinentName.AFRICA, null, null, null);
        final Territory madagascar = new Territory(24, "Madagascar", ContinentName.AFRICA, null, null, null);
        final Territory northAfrica = new Territory(25, "North Africa", ContinentName.AFRICA, null, null, null);
        final Territory southAfrica = new Territory(26, "South Africa", ContinentName.AFRICA, null, null, null);

        final Territory afghanistan = new Territory(27, "Afghanistan", ContinentName.ASIA, null, null, null);
        final Territory china = new Territory(28, "China", ContinentName.ASIA, null, null, null);
        final Territory india = new Territory(29, "India", ContinentName.ASIA, null, null, null);
        final Territory irkutsk = new Territory(30, "Irkutsk", ContinentName.ASIA, null, null, null);
        final Territory japan = new Territory(31, "Japan", ContinentName.ASIA, null, null, null);
        final Territory kamchatka = new Territory(32, "Kamchatka", ContinentName.ASIA, null, null, null);
        final Territory middleEast = new Territory(33, "Middle East", ContinentName.ASIA, null, null, null);
        final Territory mongolia = new Territory(34, "Mongolia", ContinentName.ASIA, null, null, null);
        final Territory siam = new Territory(35, "Siam", ContinentName.ASIA, null, null, null);
        final Territory ural = new Territory(36, "Ural", ContinentName.ASIA, null, null, null);
        final Territory yakutsk = new Territory(37, "Yakutsk", ContinentName.ASIA, null, null, null);
        final Territory siberia = new Territory(38, "Siberia", ContinentName.ASIA, null, null, null);

        final Territory easternAustralia = new Territory(39, "Eastern Australia", ContinentName.AUSTRALIA, null, null, null);
        final Territory indonesia = new Territory(40, "Indonesia", ContinentName.AUSTRALIA, null, null, null);
        final Territory newGuinea = new Territory(41, "New Guinea", ContinentName.AUSTRALIA, null, null, null);
        final Territory westernAustralia = new Territory(42, "Western Australia", ContinentName.AUSTRALIA, null, null, null);

        // Zapojeni do prechazivych celku
        alaska.setConnectedTerritories(new ArrayList<Territory>(){{
            add(northwestUsa);
            add(alberta);
            add(kamchatka);
        }});

        alberta.setConnectedTerritories(new ArrayList<Territory>(){{
            add(northwestUsa);
            add(ontario);
            add(westernUsa);
        }});

        centralAmerica.setConnectedTerritories(new ArrayList<Territory>(){{
            add(westernUsa);
            add(easternUsa);
            add(venezuela);
        }});

        easternUsa.setConnectedTerritories(new ArrayList<Territory>(){{
            add(centralAmerica);
            add(westernUsa);
            add(ontario);
            add(quebec);
        }});

        greenland.setConnectedTerritories(new ArrayList<Territory>(){{
            add(northwestUsa);
            add(ontario);
            add(quebec);
            add(iceland);
        }});

        northwestUsa.setConnectedTerritories(new ArrayList<Territory>(){{
            add(alaska);
            add(alberta);
            add(ontario);
            add(greenland);
        }});

        ontario.setConnectedTerritories(new ArrayList<Territory>(){{
            add(northwestUsa);
            add(alberta);
            add(westernUsa);
            add(easternUsa);
            add(quebec);
        }});

        quebec.setConnectedTerritories(new ArrayList<Territory>(){{
            add(ontario);
            add(easternUsa);
            add(greenland);
        }});

        westernUsa.setConnectedTerritories(new ArrayList<Territory>(){{
            add(alberta);
            add(easternUsa);
            add(centralAmerica);
        }});



        argentina.setConnectedTerritories(new ArrayList<Territory>(){{
            add(peru);
            add(brazil);
        }});

        brazil.setConnectedTerritories(new ArrayList<Territory>(){{
            add(argentina);
            add(peru);
            add(venezuela);
        }});

        peru.setConnectedTerritories(new ArrayList<Territory>(){{
            add(argentina);
            add(brazil);
            add(venezuela);
        }});

        venezuela.setConnectedTerritories(new ArrayList<Territory>(){{
            add(brazil);
            add(peru);
        }});



        greatBritain.setConnectedTerritories(new ArrayList<Territory>(){{
            add(iceland);
            add(scandinavia);
            add(northernEurope);
            add(westernEurope);
        }});

        iceland.setConnectedTerritories(new ArrayList<Territory>(){{
            add(greatBritain);
            add(scandinavia);
            add(greenland);
        }});

        northernEurope.setConnectedTerritories(new ArrayList<Territory>(){{
            add(southernEurope);
            add(westernEurope);
            add(ukraine);
            add(greatBritain);
            add(scandinavia);
        }});

        scandinavia.setConnectedTerritories(new ArrayList<Territory>(){{
            add(iceland);
            add(greatBritain);
            add(northernEurope);
            add(ukraine);
        }});

        southernEurope.setConnectedTerritories(new ArrayList<Territory>(){{
            add(northernEurope);
            add(westernAustralia);
            add(ukraine);
            add(northAfrica);
            add(egypt);
            add(middleEast);
        }});

        ukraine.setConnectedTerritories(new ArrayList<Territory>(){{
            add(southernEurope);
            add(scandinavia);
            add(northernEurope);
            add(ural);
            add(afghanistan);
            add(middleEast);
        }});

        westernEurope.setConnectedTerritories(new ArrayList<Territory>(){{
            add(northAfrica);
            add(greatBritain);
            add(northernEurope);
            add(southernEurope);
        }});


        congo.setConnectedTerritories(new ArrayList<Territory>(){{
            add(eastAfrica);
            add(southAfrica);
            add(northAfrica);
        }});

        eastAfrica.setConnectedTerritories(new ArrayList<Territory>(){{
            add(congo);
            add(southAfrica);
            add(madagascar);
            add(middleEast);
            add(egypt);
        }});

        egypt.setConnectedTerritories(new ArrayList<Territory>(){{
            add(southernEurope);
            add(middleEast);
            add(eastAfrica);
        }});

        madagascar.setConnectedTerritories(new ArrayList<Territory>(){{
            add(eastAfrica);
            add(southAfrica);
        }});

        northAfrica.setConnectedTerritories(new ArrayList<Territory>(){{
            add(brazil);
            add(congo);
            add(egypt);
            add(eastAfrica);
            add(middleEast);
            add(southernEurope);
        }});

        southAfrica.setConnectedTerritories(new ArrayList<Territory>(){{
            add(madagascar);
            add(congo);
            add(eastAfrica);
        }});



        afghanistan.setConnectedTerritories(new ArrayList<Territory>(){{
            add(ukraine);
            add(middleEast);
            add(india);
            add(china);
            add(ural);
        }});

        china.setConnectedTerritories(new ArrayList<Territory>(){{
            add(afghanistan);
            add(india);
            add(siam);
            add(mongolia);
            add(siberia);
            add(ural);
        }});

        india.setConnectedTerritories(new ArrayList<Territory>(){{
            add(middleEast);
            add(afghanistan);
            add(china);
            add(siam);
        }});

        irkutsk.setConnectedTerritories(new ArrayList<Territory>(){{
            add(siberia);
            add(mongolia);
            add(yakutsk);
            add(kamchatka);
        }});

        this.territories.add(alaska);
        this.territories.add(alberta);
        this.territories.add(centralAmerica);
        this.territories.add(easternUsa);
        this.territories.add(greenland);
        this.territories.add(northwestUsa);
        this.territories.add(ontario);
        this.territories.add(quebec);
        this.territories.add(westernUsa);

        this.territories.add(argentina);
        this.territories.add(brazil);
        this.territories.add(peru);
        this.territories.add(venezuela);

        this.territories.add(greatBritain);
        this.territories.add(iceland);
        this.territories.add(northernEurope);
        this.territories.add(scandinavia);
        this.territories.add(southernEurope);
        this.territories.add(ukraine);
        this.territories.add(westernEurope);

        this.territories.add(congo);
        this.territories.add(eastAfrica);
        this.territories.add(egypt);
        this.territories.add(madagascar);
        this.territories.add(northAfrica);
        this.territories.add(southAfrica);

        this.territories.add(afghanistan);
        this.territories.add(china);
        this.territories.add(india);
        this.territories.add(irkutsk);
        this.territories.add(japan);
        this.territories.add(kamchatka);
        this.territories.add(middleEast);
        this.territories.add(mongolia);
        this.territories.add(siam);
        this.territories.add(ural);
        this.territories.add(yakutsk);
        this.territories.add(siberia);

        this.territories.add(easternAustralia);
        this.territories.add(indonesia);
        this.territories.add(newGuinea);
        this.territories.add(westernAustralia);
    }
}
