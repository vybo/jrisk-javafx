package objects;

/**
 * Trida reprezentujici hlavni mesto hrace.
 *
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class MainCity extends City {
    public MainCity(String name) {
        super(name);
    }

    public int getCityBonus(){
//        TODO: Kontrola pravidel ohledne bonusu hlavniho mesta.
        return 4;
    }

    public boolean isMainCity() {
        return true;
    }
}
