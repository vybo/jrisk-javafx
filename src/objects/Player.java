package objects;

import javafx.scene.paint.Color;

/**
 * Trida reprezentujici jednoho hrace.
 *
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class Player {
    private int id;
    private String name; // Jmeno hrace
    private Color color; // Barva hrace
    private int armiesToPlace;
    private boolean ready;
    // private Card cards; // Karty hrace
    // private Dice dice; // Kostky hrace

    /**
     * Konstuktor Player, vytvori noveho horace a priradi parametry
     * @param name Jmeno hrace
     * @param color Barva, kterou bude hrac reprezentovan
     * @param startingArmies Pocet armad, ktere hrac bude moc rozdelit na herni pole
     */
    public Player(int id, String name, Color color, int startingArmies) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.armiesToPlace = startingArmies;
        this.ready = false;
    }

    public Player() {
        this.id = 0;
        this.name = "Default";
        this.color = Color.BLACK;
        this.armiesToPlace = 12;
        this.ready = false;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setColor(Color color){
        this.color = color;
    }

    public void setReady(boolean ready){
        this.ready = ready;
    }

    public boolean getReady(){
        return this.ready;
    }

    public void setColorString(String color){
        switch (color) {
            case "RED":
                this.color = Color.RED;
                break;
            case "SKYBLUE":
                this.color = Color.SKYBLUE;
                break;
            case "YELLOW":
                this.color = Color.YELLOW;
                break;
            default:
                this.color = Color.WHITE;
                break;
        }
    }

    public void setArmiesToPlace(int armies){
        this.armiesToPlace = armies;
    }

    public String getName(){
        return this.name;
    }

    public Color getColor(){
        return this.color;
    }

    public String getColorString(){
        if (this.color == Color.RED){
            return "RED";
        }
        else if (this.color == Color.SKYBLUE){
            return "SKYBLUE";
        }
        else if(this.color == Color.YELLOW){
            return "YELLOW";
        }
        else {
            return "WHITE";
        }
    }

    public int getId(){
        return this.id;
    }

    public int getArmiesToPlace() {
        return this.armiesToPlace;
    }

    public boolean placeArmy(){
        if (this.armiesToPlace > 0) {
            this.armiesToPlace--;
            return true;
        }

        return false;
    }
}
