package objects;

/**
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public enum ContinentName {
    NORTH_AMERICA,
    SOUTH_AMERICA,
    EUROPE,
    AFRICA,
    ASIA,
    AUSTRALIA}
