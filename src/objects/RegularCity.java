package objects;

/**
 * Trida reprezentujici klasicke mesto na mape.
 *
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class RegularCity extends City{
    public RegularCity(String name) {
        super(name);
    }

    public int getCityBonus(){
//        TODO: Kontrola pravidel ohledne bonusu hlavniho mesta.
        return 2;
    }

    public boolean isMainCity() {
        return false;
    }
}
