package objects;

/**
 * Abstraktni trida reprezentujici mesto v teritoriu, ktere udava bonus armad.
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public abstract class City {
    private String name;

    /**
     * Konstruktor tridy prirazujici jmeno.
     *
     * @param name Jmeno mesta
     */
    public City(String name) {
        this.name = name;
    }

    /**
     * Metoda vracejici bonus armad mesta.
     *
     * @return Pocet armad, ktere mesto generuje.
     */
    public abstract int getCityBonus();

    /**
     * Metoda kontrolujici, zda je mesto hlavnim mestem.
     *
     * @return Logicka hodnota, zda je mesto hlavnim mestem
     */
    public abstract boolean isMainCity();
}
