package objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Trida reprezentujici jedno uzemi na mape.
 *
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class Territory {
    private int id;
    private String name;
    private ArrayList<Territory> connectedTerritories;
    private City city;
    private int armies;
    private Player owner;
    private ContinentName continent;

    /**
     * Konstuktor tridy.
     *
     * @param name Nazev teritoria
     * @param connectedTerritories Seznam pripojenych teritorii, kam lze poslat armady
     * @param city Mesto v teritoriu
     * @param owner Vlastnik teritoria
     */
    public Territory(int id, String name, ContinentName continent, ArrayList<Territory> connectedTerritories, City city, Player owner) {
        this.name = name;
        this.connectedTerritories = connectedTerritories;
        this.city = city;
        this.owner = owner;
        this.armies = 0;
        this.continent = continent;
        this.id = id;
    }

    /**
     * Metoda ke zjisteni nazvu teritoria.
     *
     * @return Nazev teritoria
     */
    public String getName() {
        return  this.name;
    }

    public int getId(){
        return this.id;
    }

    /**
     * Metoda kontrolujici, zda je teritorium napojeno na udane -> lze mezi nimi presunovat armady?
     *
     * @param territory Teritorium, ktere bude kontrolovano, zda je na toto napojene
     * @return Logicka hodnota udavajici, zda je teritorium napojenu na udane
     */
    public boolean isConnectedTo(Territory territory) {
        if (this.connectedTerritories.indexOf(territory) != -1)
            return true;

        return false;
    }

    /**
     * Metoda k navraceni mesta teritoria.
     *
     * @return Mesto v teritoriu.
     */
    public City getCity() {
        return this.city;
    }

    public void spawnCity() {
        this.city = new RegularCity("Nic");
    }

    /**
     * Metoda k navraceni vlastnika.
     *
     * @return Hrac vlastnici teritorium.
     */
    public Player getOwner() {
        return this.owner;
    }

    public void setOwner(Player owner){
        this.owner = owner;
    }

    /**
     * Metoda generujici bonusove armady pro vlastnika.
     *
     * @return Integer kolik armad je generovano teritoriem
     */
    public int generateBonusArmies() {
        // TODO: Podle pravidel generovat bonusy
        return 2;
    }

    /**
     * Metoda ke zjisteni poctu armad v teritoriu.
     *
     * @return Integer kolik armad se v teritoriu nachazi
     */
    public int getArmies() {
        return this.armies;
    }

    public void transferArmiesOut (int howMuch){
        this.armies -= howMuch;
    }

    public void transferArmiesTo (int howMuch){
        this.armies += howMuch;
    }

    public void setConnectedTerritories(ArrayList<Territory> newConnectedTerritories){
        this.connectedTerritories = newConnectedTerritories;
    }
}
