package objects;

import java.util.Random;

/**
 * Staticka trida Dice, ktera reprezentuje herni kostku.
 *
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public final class Dice {
    private static Random randomGenerator;

    /**
     * Konstruktor delajici staticnost tridy.
     */
    private Dice() {
        randomGenerator = new Random();
    }

    /**
     * Hod kostkou.
     * @return Vraci integer hodnotu mezi 1 a 6.
     */
    public static int Roll() {
        return randomGenerator.nextInt(5) + 1; // Aby byl range od 1 do 6
    }
}
