package objects;

/**
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public enum Stage {
    DRAFT,
    ATTACK,
    FORTIFY,
    SETUP
}
