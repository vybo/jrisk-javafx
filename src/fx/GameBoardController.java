package fx; /**

 * Sample Skeleton for 'GameBoard.fxml' Controller Class
 */

import fx.controls.CountryBlob;
import javafx.animation.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;
import objects.Player;
import objects.Stage;
import system.Client;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class GameBoardController extends SceneController implements Initializable{
    @FXML
    private ImageView stageStatus;

    @FXML
    private GridPane gridNickRight;

    @FXML
    private Text txtPl1Name;

    @FXML
    private ImageView nickLeft;

    @FXML
    private ImageView background;

    @FXML
    private GridPane gridStageStatus;

    @FXML
    private ImageView worldMap;

    @FXML
    private Text txtPl2Name;

    @FXML
    private GridPane gridNickLeft;

    @FXML
    private Text txtStageStatus;

    @FXML
    private GridPane mainGrid;

    @FXML
    private ImageView buttonMenu;

    @FXML
    private ImageView nickRight;

    @FXML
    private Pane paneBlobs;

    @FXML
    private CountryBlob blob1;

    @FXML
    private CountryBlob blob2;

    @FXML
    private CountryBlob blob3;

    @FXML
    private CountryBlob blobAlaska;

    @FXML
    private CountryBlob blobNorthWestUsa;

    @FXML
    private CountryBlob blobAlberta;

    @FXML
    private CountryBlob blobOntario;

    @FXML
    private CountryBlob blobQuebec;

    @FXML
    private CountryBlob blobEasternUsa;

    @FXML
    private CountryBlob blobWesternUsa;

    @FXML
    private CountryBlob blobCentralUsa;

    @FXML
    private CountryBlob blobVenezuela;

    @FXML
    private CountryBlob blobBrazil;

    @FXML
    private CountryBlob blobPeru;

    @FXML
    private CountryBlob blobArgentina;

    @FXML
    private CountryBlob blobGreenland;

    @FXML
    private CountryBlob blobScandinavia;

    @FXML
    private CountryBlob blobUkraine;

    @FXML
    private CountryBlob blobBritain;

    @FXML
    private CountryBlob blobNorthernEurope;

    @FXML
    private CountryBlob blobWesternEurope;

    @FXML
    private CountryBlob blobSouthernEurope;

    @FXML
    private CountryBlob blobUral;

    @FXML
    private CountryBlob blobAfghanistan;

    @FXML
    private CountryBlob blobSiberiaß;

    @FXML
    private CountryBlob blobYakutsk;

    @FXML
    private CountryBlob blobKamchatka;

    @FXML
    private CountryBlob blobIrkutsk;

    @FXML
    private CountryBlob blobMongolia;

    @FXML
    private CountryBlob blobJapan;

    @FXML
    private CountryBlob blobIndia;

    @FXML
    private CountryBlob blobChina;

    @FXML
    private CountryBlob blobSiam;

    @FXML
    private CountryBlob blobMiddleEast;

    @FXML
    private CountryBlob blobNorthAfrica;

    @FXML
    private CountryBlob blobEgypt;

    @FXML
    private CountryBlob blobEastAfrica;

    @FXML
    private CountryBlob blobSouthAfrica;

    @FXML
    private CountryBlob blobCongo;

    @FXML
    private CountryBlob blobMadagascar;

    @FXML
    private CountryBlob blobIndonesia;

    @FXML
    private CountryBlob blobNewGuinea;

    @FXML
    private CountryBlob blobWesternAustralia;

    @FXML
    private CountryBlob blobEasternAustralia;

    @FXML
    private CountryBlob blobIceland;

    @FXML
    private Path transferPath;

    @FXML
    private CountryBlob transferBlob;

    @FXML
    private GridPane notification;

    @FXML
    private Text notificationText;

    @FXML
    private Rectangle notificationOk;

    @FXML
    private Text button1Text;

    @FXML
    private TextArea consoleOut;

    @FXML
    private TextField consoleIn;

    @FXML
    private GridPane console;

    private CountryBlob selectedCountryFrom = null;

    private CountryBlob selectedCountryTo = null;

    private Client gameClient = Client.getCurrent();

    private Player player1;
    private Player player2;
    private Lighting player1Lighting;
    private Lighting player2Lighting;

    private void transferArmiesAnimation(CountryBlob from, CountryBlob to){
        transferBlob.setArmies(0);

        Light.Distant light = new Light.Distant();
        light.setAzimuth(45.0);
        light.setColor(Color.RED);
        Lighting lighting = new Lighting();
        lighting.setLight(light);
        lighting.setSurfaceScale(1.5);
        transferBlob.setBlobEffect(lighting);


        ScaleTransition st1 = new ScaleTransition(Duration.millis(500d), transferBlob);
        st1.setFromX(0d);
        st1.setFromY(0d);
        st1.setFromZ(0d);
        st1.setToX(1d);
        st1.setToZ(1d);
        st1.setToY(1d);
        st1.setCycleCount(1);
        st1.setAutoReverse(false);

        // +18 aby path byla uprostred blobu - polomer blobu je 18
        transferPath.getElements().clear();
        transferPath.getElements().add(new MoveTo(from.getLayoutX() + 18, from.getLayoutY() + 18));

        ArcTo arcTo = new ArcTo();
        arcTo.setX(to.getLayoutX() + 18);
        arcTo.setY(to.getLayoutY() + 18);
        arcTo.setRadiusX(5f);
        arcTo.setRadiusY(2f);
        double axisRot = Math.toDegrees(Math.atan2(to.getLayoutY() - from.getLayoutY(), to.getLayoutX() - from.getLayoutX()));
        axisRot = (axisRot < 0) ? axisRot += 360 : axisRot;
        arcTo.setXAxisRotation(axisRot);
        /*arcTo.setLargeArcFlag(false);
        arcTo.setSweepFlag(true);*/

        transferPath.getElements().add(arcTo);
        transferPath.setStrokeWidth(2);
        transferPath.setStroke(Color.YELLOW);
        transferPath.setOpacity(0d);

        FadeTransition ft1 = new FadeTransition(Duration.millis(500d), transferPath);
        ft1.setToValue(1.0d);
        ft1.setFromValue(0.0d);
        ft1.setCycleCount(1);
        ft1.setAutoReverse(false);

        PathTransition pathTransition = PathTransitionBuilder.create()
                .node(transferBlob)
                .path(transferPath)
                .duration(Duration.millis(2000))
                .orientation(PathTransition.OrientationType.NONE)
                .cycleCount(1)
                .autoReverse(false)
                .build();

        ScaleTransition st2 = new ScaleTransition(Duration.millis(500d), transferBlob);
        st2.setFromX(1d);
        st2.setFromY(1d);
        st2.setFromZ(1d);
        st2.setToX(0d);
        st2.setToZ(0d);
        st2.setToY(0d);
        st2.setCycleCount(1);
        st2.setAutoReverse(false);

        FadeTransition ft2 = new FadeTransition(Duration.millis(500d), transferPath);
        ft2.setToValue(0.0d);
        ft2.setFromValue(1.0d);
        ft2.setCycleCount(1);
        ft2.setAutoReverse(false);

        SequentialTransition sq =  new SequentialTransition();
        sq.getChildren().addAll(ft1, st1, pathTransition, st2, ft2);
        sq.setCycleCount(1);
        sq.setAutoReverse(true);
        sq.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stageDone(); // TODO: This makes it switch twice
            }
        });
        sq.play();

        this.selectedCountryFrom = null;
        this.selectedCountryTo = null;
    }

    @FXML
    void blobMouseEntered (javafx.scene.input.MouseEvent event) {
        Object blob = event.getSource();
        if (blob instanceof CountryBlob) {
            ScaleTransition st = new ScaleTransition(Duration.millis(500d), (CountryBlob)blob);
            st.setFromX(1d);
            st.setFromY(1d);
            st.setFromZ(1d);
            st.setToX(1.3d);
            st.setToZ(1.3d);
            st.setToY(1.3d);
            st.setCycleCount(1);
            st.setAutoReverse(false);
            st.play();
        }

    }

    @FXML
    void blobMouseExited(javafx.scene.input.MouseEvent event) {
        Object blob = event.getSource();
        if (blob instanceof CountryBlob) {
            ScaleTransition st = new ScaleTransition(Duration.millis(500d), (CountryBlob) blob);
            st.setFromX(1.3d);
            st.setFromY(1.3d);
            st.setFromZ(1.3d);
            st.setToX(1d);
            st.setToZ(1d);
            st.setToY(1d);
            st.setCycleCount(1);
            st.setAutoReverse(false);
            st.play();
        }
    }

    @FXML
    void blobMousePressed(javafx.scene.input.MouseEvent event) {

    }

    @FXML
    void blobMouseReleased(javafx.scene.input.MouseEvent event) {
        Object test = event.getSource();
        if (test instanceof CountryBlob) {
            ((CountryBlob)test).incrementArmies();
            if (this.selectedCountryFrom == null){
                this.selectedCountryFrom = (CountryBlob)test;
                System.out.println("Selected FROM blob ID " + ((CountryBlob) test).getBlobId());
            }
            else if (this.selectedCountryTo == null){
                this.selectedCountryTo = (CountryBlob)test;
                gameClient.sendAttack(this.selectedCountryFrom.getBlobId(), this.selectedCountryTo.getBlobId());;
                //this.transferArmiesAnimation(this.selectedCountryFrom, this.selectedCountryTo);
                System.out.println("Selected TO blob ID " + ((CountryBlob) test).getBlobId());
                selectedCountryTo = null;
                selectedCountryFrom = null;
            }
        }
    }

    public void attackOccured(int territoryIdFrom, int territoryIdTo) {
        // TODO: optimize this, also dangerous type conversion
        for (javafx.scene.Node bl : paneBlobs.getChildren()) {
            if (bl instanceof CountryBlob){
                if (territoryIdFrom != -1 && ((CountryBlob) bl).getBlobId() == territoryIdFrom) {
                    selectedCountryFrom = ((CountryBlob) bl);
                    territoryIdFrom = -1;
                }
                else if (territoryIdTo != -1 && ((CountryBlob) bl).getBlobId() == territoryIdTo) {
                    if (territoryIdTo != -1 && ((CountryBlob) bl).getBlobId() == territoryIdTo) {
                        selectedCountryTo = ((CountryBlob) bl);
                    }
                }
            }
        }
        if (selectedCountryTo != null && selectedCountryFrom != null)
            this.transferArmiesAnimation(selectedCountryFrom, selectedCountryTo);
    }

    @FXML
    void onButtonBackPressed() {
        buttonMenu.setImage(new Image("uirenders/DotButtonPressed.png"));
    }

    @FXML
    void onButtonBackReleased() throws IOException {
        buttonMenu.setImage(new Image("uirenders/DotButton.png"));
        sceneManager.navigate("MainMenu.fxml");
    }

    @FXML
    void notificationOkPressed() {

    }

    @FXML
    void notificationOkReleased() {

    }

    public void showNotification(String text){

    }

    public void changeGameStage(Stage newStage, int newPlayerIdOnStage){
        //TODO: Check error neexistujiciho id?
        int depth = 70; //Setting the uniform variable for the glow width and height

        DropShadow borderGlow = new DropShadow();
        borderGlow.setOffsetY(0f);
        borderGlow.setOffsetX(0f);
        borderGlow.setColor(Color.YELLOW);
        borderGlow.setWidth(50);
        borderGlow.setHeight(50);

        if (newPlayerIdOnStage == player1.getId()){
            borderGlow.setInput(player1Lighting);
            nickLeft.setEffect(borderGlow);
            nickRight.setEffect(player2Lighting);
        }
        else if (newPlayerIdOnStage == player2.getId()){
            borderGlow.setInput(player2Lighting);
            nickRight.setEffect(borderGlow);
            nickLeft.setEffect(player1Lighting);

        }
        else {
            borderGlow.setColor(Color.RED);
            nickLeft.setEffect(borderGlow);
            nickRight.setEffect(borderGlow);
        }

        switch (newStage){
            case SETUP:
                txtStageStatus.setText("Setup");
                break;
            case ATTACK:
                txtStageStatus.setText("Attack");
                break;
            case DRAFT:
                txtStageStatus.setText("Draft");
                break;
            case FORTIFY:
                txtStageStatus.setText("Fortify");
                break;
            default:
                txtStageStatus.setText("Broken");
                break;
        }
    }

    // Metoda stanouci se po zmacknuti tlacitka odsouhlaseni tahu
    private void stageDone() {
        try {
            gameClient.stageDone();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @FXML // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL location, ResourceBundle resources) {
        assert stageStatus != null : "fx:id=\"stageStatus\" was not injected: check your FXML file 'GameBoard.fxml'.";
        assert worldMap != null : "fx:id=\"worldMap\" was not injected: check your FXML file 'GameBoard.fxml'.";
        assert mainGrid != null : "fx:id=\"mainGrid\" was not injected: check your FXML file 'GameBoard.fxml'.";
        assert background != null : "fx:id=\"background\" was not injected: check your FXML file 'GameBoard.fxml'.";
        assert buttonMenu != null : "fx:id=\"buttonMenu\" was not injected: check your FXML file 'GameBoard.fxml'.";
        assert nickLeft != null : "fx:id=\"nickLeft\" was not injected: check your FXML file 'GameBoard.fxml'.";
        assert nickRight != null : "fx:id=\"nickRight\" was not injected: check your FXML file 'GameBoard.fxml'.";

        sceneManager = SceneManager.getCurrent();
        gameClient.setControlledScreen(this);

        FadeTransition ft = new FadeTransition(Duration.millis(500d), mainGrid);
        ft.setToValue(1.0d);
        ft.setFromValue(0.0d);
        ft.setCycleCount(1);

        ScaleTransition st = new ScaleTransition(Duration.millis(500d), worldMap);
        st.setFromX(0d);
        st.setFromY(0d);
        st.setFromZ(0d);
        st.setToX(1d);
        st.setToZ(1d);
        st.setToY(1d);
        st.setCycleCount(1);

        TranslateTransition tt1 = new TranslateTransition(Duration.millis(500d), gridNickLeft);
        tt1.setFromX(-300d);
        tt1.setToX(0d);
        tt1.setCycleCount(1);
        tt1.setAutoReverse(false);

        TranslateTransition tt2 = new TranslateTransition(Duration.millis(500d), gridNickRight);
        tt2.setFromX(400d);
        tt2.setToX(0d);
        tt2.setCycleCount(1);
        tt2.setAutoReverse(false);

        TranslateTransition tt3 = new TranslateTransition(Duration.millis(500d), gridStageStatus);
        tt3.setFromY(-90d);
        tt3.setToY(0d);
        tt3.setCycleCount(1);
        tt3.setAutoReverse(false);

        TranslateTransition tt4 = new TranslateTransition(Duration.millis(500d), buttonMenu);
        tt4.setFromX(-90d);
        tt4.setToX(0d);
        tt4.setCycleCount(1);
        tt4.setAutoReverse(false);

        FadeTransition ft2 = new FadeTransition(Duration.millis(500d), paneBlobs);
        ft2.setToValue(1d);
        ft2.setFromValue(0d);
        ft.setCycleCount(1);

        // Pokud nepockame na prehrati animace, vznikne exception o uprave bindovane property
        st.onFinishedProperty().set(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                worldMap.scaleXProperty().bind(mainGrid.widthProperty().divide(1280));
                worldMap.scaleYProperty().bind(mainGrid.heightProperty().divide(720));
            }
        });

        SequentialTransition sq =  new SequentialTransition();
        sq.getChildren().addAll(ft, st, tt1, tt2, tt3, tt4, ft2);
        sq.setCycleCount(1);
        sq.setAutoReverse(false);
        sq.play();

        paneBlobs.scaleXProperty().bind(mainGrid.widthProperty().divide(1280));
        paneBlobs.scaleYProperty().bind(mainGrid.heightProperty().divide(720));
        background.scaleXProperty().bind(mainGrid.widthProperty().divide(1280));
        background.scaleYProperty().bind(mainGrid.heightProperty().divide(720));

        try {
            player1 = gameClient.getGame().getFirstPlayer();
            player2 = gameClient.getGame().getSecondPlayer();
            txtPl1Name.setText(player1.getName());
            txtPl2Name.setText(player2.getName());

            Light.Distant light = new Light.Distant();
            light.setAzimuth(45.0);
            light.setColor(player1.getColor());
            Lighting lighting = new Lighting();
            lighting.setLight(light);
            lighting.setSurfaceScale(1.5);
            player1Lighting = lighting;
            nickLeft.setEffect(player1Lighting);
            light = new Light.Distant();
            light.setAzimuth(45.0);
            light.setColor(player2.getColor());
            lighting = new Lighting();
            lighting.setLight(light);
            lighting.setSurfaceScale(1.5);
            player2Lighting = lighting;
            nickRight.setEffect(player2Lighting);
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("This is for debugging purposes and should not be in the code anymore.");
            ex.printStackTrace();
        }
    }

    @FXML
    void consoleOnKeyPressed(javafx.scene.input.KeyEvent event) {
        if (KeyCode.CAPS.equals(event.getCode())) {
            this.toggleConsole();
        }
    }

    @FXML
    void consoleInOnKeyPressed(javafx.scene.input.KeyEvent event) {
        if (KeyCode.ENTER.equals(event.getCode())) {
            String command = consoleIn.getText();
            this.displayConsoleMessage("Parsing command " + command + "...\n");
            this.parseConsoleCommand(command);
            //this.consoleIn.setText("");
            this.consoleIn.clear();
        }
    }

    private void toggleConsole(){
        TranslateTransition tt1 = new TranslateTransition(Duration.millis(200d), console);
        tt1.setCycleCount(1);
        tt1.setAutoReverse(false);

        if (console.getTranslateY() < 0){
            tt1.setFromY(-270);
            tt1.setToY(0d);
        }
        else{
            tt1.setFromY(0d);
            tt1.setToY(-270d);
        }

        tt1.play();
    }

    private void parseConsoleCommand(String command){
        String[] fullCommand = new String[6];
        fullCommand = command.split(" ", -1);

        if(fullCommand[0] != null){
            switch(fullCommand[0].toUpperCase()){
                case "PING":
                    if (this.gameClient != null) {
                        try {
                            gameClient.sendCustomCommand("PING");
                            this.displayConsoleMessage("Ping sent.\n");
                        } catch (IOException ex){
                            this.displayConsoleMessage("Ping couldn't be sent.\n");
                            ex.printStackTrace();
                        }
                    }
                    else{
                        this.displayConsoleMessage("Not connected to any server.\n");
                    }
                    break;
                case "CMD":
                    if (this.gameClient != null) {
                        try {
                            gameClient.sendCustomCommand(command.toUpperCase());
                            this.displayConsoleMessage("Command sent.\n");
                        } catch (IOException ex){
                            this.displayConsoleMessage("Command couldn't be sent.\n");
                            ex.printStackTrace();
                        }
                    }
                    else{
                        this.displayConsoleMessage("Not connected to any server.\n");
                    }
                    break;
                case "CLEAR":
                    consoleOutBuilder = new StringBuilder();
                    consoleOut.setText(consoleOutBuilder.toString());
                    break;
                default:
                    this.displayConsoleMessage("Unknown command. Refer to project's wiki.\n");
                    break;
            }
        }
        else {
            this.displayConsoleMessage("Couldn't parse input.\n");
        }
    }

    private StringBuilder consoleOutBuilder = new StringBuilder();

    public void displayConsoleMessage(String message){
        consoleOutBuilder.append(message);
        consoleOut.setText(consoleOutBuilder.toString());
        consoleOut.setScrollTop(Double.MAX_VALUE);
    }
}
