/**
 * Sample Skeleton for 'CountryBlob.fxml' Controller Class
 */

package fx.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.effect.Effect;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CountryBlob extends GridPane {
    private int armies;

    @FXML
    private int blobId;

    public CountryBlob() {
        FXMLLoader fxmlLoader = new FXMLLoader(
                getClass().getResource("CountryBlob.fxml"));

        fxmlLoader.setRoot(this); fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        this.armies = 0;
        //this.blobId = 0;
    }

    public int getBlobId(){
        return this.blobId;
    }

    public void setBlobId(int id){
        this.blobId = id;
    }

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="blob"
    private Ellipse blob; // Value injected by FXMLLoader

    @FXML // fx:id="txtArmies"
    private Text txtArmies; // Value injected by FXMLLoader

    public void setArmies(int toWhat) {
        this.armies = toWhat;
        this.txtArmies.setText(Integer.toString(this.armies));
    }

    public void incrementArmies() {
        this.armies++;
        this.txtArmies.setText(Integer.toString(this.armies));
    }

    public void setBlobEffect(Effect effect){
        blob.setEffect(effect);
    }

    @FXML
    void blobMouseEntered (javafx.scene.input.MouseEvent event) {

    }

    @FXML
    void blobMouseExited(javafx.scene.input.MouseEvent event) {

    }

    @FXML
    void blobMousePressed(javafx.scene.input.MouseEvent event) {

    }

    @FXML
    void blobMouseReleased(javafx.scene.input.MouseEvent event) {

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert blob != null : "fx:id=\"blob\" was not injected: check your FXML file 'CountryBlob.fxml'.";
        assert txtArmies != null : "fx:id=\"txtArmies\" was not injected: check your FXML file 'CountryBlob.fxml'.";

    }
}
