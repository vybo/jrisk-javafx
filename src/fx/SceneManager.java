package fx;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Jedinacek slouzici pro navigaci mezi ruznymi obrazovkami - scenami.
 *
 * Obsahuje SceneStack - seznam scen ve kterych se uzivatel nachazel a muze se na ne
 * zpet navigovat.
 *
 * Created by danvybiral on 27/10/14.
 */
public class SceneManager {
    private static SceneManager current;

    private FXMLLoader stageLoader;
    private Pane mainPane;
    private SceneController currentController;
    private Scene currentScene;
    private List<Scene> previousSceneStack = new ArrayList<Scene>();
    private Stage primaryStage;

    private SceneManager(){

    }

    public static SceneManager getCurrent(){
        //Je-li promenna instance null, tak se vytvori objekt
        if (current == null) {
            current = new SceneManager();
        }

        //Vratime jedinacka
        return current;
    }

    /**
     * Metoda slouzici k nastaveni privatni promenne hlavni Stage. Je to potreba, aby
     * SceneManager mohl spravovat aktualne zobrazenou scenu misto hlavni metody start
     * ve tride Main, ktera nemuze byt staticka a tudiz k ni nelze pristoupit z jinych
     * trid - obrazovek.
     *
     * @param stage Hlavni Stage okna, ve ktere se hostuji sceny
     */
    public void SetStage(Stage stage){
        primaryStage = stage;
        primaryStage.show();
    }



    /**
     * Metoda slouzici k navigaci vpred na jinou scenu. Ulozi do SceneStacku aktualni
     * scenu a naviguje na novou.
     *
     * @param scene Scena, na kterou se bude navigovat
     * @throws IOException Pokud se nepodari nacist FXML nove sceny
     */
    public void navigate(final String scene) throws IOException{
        /* Thread safety -> toto muze byt volano z jineho vlakna, napr. ClientThreadu
           aniz by to odporovalo standardum JavyFX
        */


        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try{
                    stageLoader = new FXMLLoader(SceneManager.class.getResource(scene));
                    mainPane = (Pane)stageLoader.load();
                    currentController = (SceneController)stageLoader.getController();
                    currentScene = new Scene(mainPane);
                    previousSceneStack.add(currentScene);

                    primaryStage.setScene(currentScene);
                } catch (IOException ex){
                    System.out.println("Navigation to another scene failed!!!");
                    ex.printStackTrace();
                }
            }
        });

        /*
        stageLoader = new FXMLLoader(SceneManager.class.getResource(scene));
        mainPane = (Pane)stageLoader.load();
        currentController = (SceneController)stageLoader.getController();
        currentScene = new Scene(mainPane);
        previousSceneStack.add(currentScene);

        primaryStage.setScene(currentScene);
        */
    }

    /**
     * Metoda slouzici k navigaci zpet na predchozi scenu. Scena, na kterou se zpet
     * naviguje bude odstranena ze SceneStacku
     */
    public void goBack(){
        /*
        currentScene = previousSceneStack.get(previousSceneStack.size() - 1);
        previousSceneStack.remove(previousSceneStack.size() - 1);

        primaryStage.setScene(currentScene);
        */

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentScene = previousSceneStack.get(previousSceneStack.size() - 1);
                previousSceneStack.remove(previousSceneStack.size() - 1);

                primaryStage.setScene(currentScene);
            }
        });
    }
}
