package fx;

import javafx.scene.Scene;

/**
 * Abstraktni trida slouzici k dosazovani instanci ve SceneManageru.
 *
 * Created by danvybiral on 27/10/14.
 */
public abstract class SceneController {
    protected SceneManager sceneManager;

    public SceneController(){
        sceneManager.getCurrent();
    }
}
