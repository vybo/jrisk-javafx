package fx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("JRisk!");
        /*
        FXMLLoader stageLoader = new FXMLLoader(getClass().getResource("MainMenu.fxml"));
        //Parent root = FXMLLoader.load(getClass().getResource("MainMenu.fxml"));
        Pane mainPane = (Pane)stageLoader.load();
        MainMenuController controller = (MainMenuController) stageLoader.getController();
        controller.setPrevStage(primaryStage);

        Scene mainScene = new Scene(mainPane);
        primaryStage.setScene(mainScene);
        primaryStage.show();

        /*
        primaryStage.setScene(new Scene(root));
        primaryStage.show();*/
/*
        primaryStage.setScene(SceneManager.goForward("MainMenu.fxml"));
        primaryStage.show();
  */
        SceneManager sceneManager = SceneManager.getCurrent();
        sceneManager.SetStage(primaryStage);
        sceneManager.navigate("MainMenu.fxml");
    }

    public static void main(String[] args) {
        launch(args);
    }
}