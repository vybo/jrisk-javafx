/**
 * Sample Skeleton for 'GameSetup.fxml' Controller Class
 */

package fx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;
import javafx.util.Duration;
import objects.Player;
import system.Client;
import system.Server;

public class GameSetupController extends SceneController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ImageView buttonMenu;

    @FXML
    private Polygon polygonArrow;

    @FXML
    private TextArea txtAreaNetgame;

    @FXML
    private ImageView btnChangeColorOne;

    @FXML
    private Button btnConnect;

    @FXML
    private TextField txInPlayerTwo;

    @FXML
    private ImageView btnDiffEasy;

    @FXML
    private GridPane mainGrid;

    @FXML
    private Text btnDiffHardText;

    @FXML
    private ImageView btnAiGame;

    @FXML
    private ImageView bgImage;

    @FXML
    private ImageView btnDiffHard;

    @FXML
    private ImageView nickLeft;

    @FXML
    private TextField txtBoxIpPort;

    @FXML
    private ImageView nickRight;

    @FXML
    private ImageView btnSeatGame;

    @FXML
    private Text btnPl2ColorText;

    @FXML
    private Text textPlayerTwo;

    @FXML
    private TextField txtBoxNetworkInput;

    @FXML
    private Text textPlayerOne;

    @FXML
    private ImageView btnNetGame;

    @FXML
    private ImageView btnChangeColorTwo;

    @FXML
    private TextField txInPlayerOne;

    @FXML
    private Text btnDiffEasyText;

    @FXML
    private Text txtIpPort;

    @FXML
    private CheckBox chkboxServer;

    @FXML
    private ImageView btnDone;

    @FXML
    private Text btnDoneText;

    @FXML
    private GridPane gridHotSeatProperties;

    @FXML
    private GridPane gridAiProperties;

    @FXML
    private GridPane gridNetworkProperties;

    @FXML
    private GridPane gridBubble;

    @FXML
    private GridPane gridGamePropertiesParent;

    private enum GameMode {
        AI, HOTSEAT, NETWORK
    }

    private GameMode mode = GameMode.AI;

    private Player playerOne = new Player();
    private Player playerTwo = new Player();

    @FXML
    void onButtonBackPressed() {
        buttonMenu.setImage(new Image("uirenders/DotButtonPressed.png"));
    }

    @FXML
    void onButtonBackReleased() throws IOException {
        buttonMenu.setImage(new Image("uirenders/DotButton.png"));
        sceneManager.navigate("MainMenu.fxml");
    }

    @FXML
    void txInPlayerOneReleased() {

    }

    @FXML
    void txInPlayerOneClicked() {

    }

    @FXML
    void txInPlayerTwoReleased() {

    }

    @FXML
    void txInPlayerTwoClicked() {

    }

    @FXML
    void txInPlayerOneChanged() {
        textPlayerOne.setText(txInPlayerOne.getText());
        playerOne.setName(txInPlayerOne.getText());
    }

    @FXML
    void txInPlayerTwoChanged() {
        textPlayerTwo.setText(txInPlayerTwo.getText());
        playerTwo.setName(txInPlayerTwo.getText());

    }

    @FXML
    void btnChangeColorOnePressed() {
        btnChangeColorOne.setImage(new Image("uirenders/MenuBtnPressed.png"));
    }

    @FXML
    void btnChangeColorOneReleased() {
        btnChangeColorOne.setImage(new Image("uirenders/MenuBtnActive.png"));

        Light.Distant light = new Light.Distant();
        light.setAzimuth(45.0);

        Color current = ((Lighting)nickLeft.getEffect()).getLight().getColor();

        if (current == Color.SKYBLUE) {
            current = Color.RED;
        }
        else if (current == Color.RED){
            current = Color.YELLOW;
        }
        else {
            current = Color.SKYBLUE;
        }

        light.setColor(current);
        Lighting lighting = new Lighting();
        lighting.setLight(light);
        lighting.setSurfaceScale(1.5);

        playerOne.setColor(current);

        nickLeft.setEffect(lighting);
    }

    @FXML
    void btnChangeColorTwoPressed() {
        btnChangeColorTwo.setImage(new Image("uirenders/MenuBtnPressed.png"));
    }

    @FXML
    void btnChangeColorTwoReleased() {
        btnChangeColorTwo.setImage(new Image("uirenders/MenuBtnActive.png"));

        Light.Distant light = new Light.Distant();
        light.setAzimuth(45.0);

        Color current = ((Lighting)nickRight.getEffect()).getLight().getColor();

        if (current == Color.SKYBLUE) {
            current = Color.RED;
        }
        else if (current == Color.RED){
            current = Color.YELLOW;
        }
        else {
            current = Color.SKYBLUE;
        }

        light.setColor(current);
        Lighting lighting = new Lighting();
        lighting.setLight(light);
        lighting.setSurfaceScale(1.5);

        playerTwo.setColor(current);
        nickRight.setEffect(lighting);
    }

    @FXML
    void btnDonePressed() {
        btnDone.setImage(new Image("uirenders/MenuBtnPressed.png"));

    }

    @FXML
    void btnDoneReleased() throws IOException {
        btnDone.setImage(new Image("uirenders/MenuBtnActive.png"));

        try{
            gameClient.playerReady();
        } catch (IOException ex){
            this.printMessage("Couldn't ready you.\n");
            ex.printStackTrace();
        }

        //this.gotoGameBoard();
    }

    void enableBtnDone(){
        btnDone.setImage(new Image("uirenders/MenuBtnActive.png"));
        btnDone.setMouseTransparent(false);
    }

    void disableBtnDone(){
        btnDone.setImage(new Image("uirenders/MenuBtnInactive.png"));
        btnDone.setMouseTransparent(true);
    }

    @FXML
    void btnAiGamePressed() {
        btnAiGame.setImage(new Image("uirenders/MenuBtnPressed.png"));
    }

    @FXML
    void btnAiGameReleased() {
        btnAiGame.setImage(new Image("uirenders/MenuBtnActive.png"));

        ScaleTransition st1 = new ScaleTransition(Duration.millis(500d), gridGamePropertiesParent);
        st1.setFromX(1d);
        st1.setFromY(1d);
        st1.setFromZ(1d);
        st1.setToX(0d);
        st1.setToZ(0d);
        st1.setToY(0d);
        st1.setCycleCount(1);
        st1.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                gridAiProperties.setVisible(true);
                gridAiProperties.setMouseTransparent(false);
                gridHotSeatProperties.setVisible(false);
                gridHotSeatProperties.setMouseTransparent(true);
                gridNetworkProperties.setVisible(false);
                gridNetworkProperties.setMouseTransparent(true);
            }
        });

        TranslateTransition tt1 = new TranslateTransition(Duration.millis(300d), polygonArrow);
        tt1.setFromY(polygonArrow.getTranslateY());
        tt1.setToY(-95d);
        tt1.setCycleCount(1);
        tt1.setAutoReverse(false);

        ScaleTransition st2 = new ScaleTransition(Duration.millis(500d), gridGamePropertiesParent);
        st2.setFromX(0d);
        st2.setFromY(0d);
        st2.setFromZ(0d);
        st2.setToX(1d);
        st2.setToZ(1d);
        st2.setToY(1d);
        st2.setCycleCount(1);


        SequentialTransition sq =  new SequentialTransition();
        sq.getChildren().addAll(st1,tt1, st2);
        sq.setCycleCount(1);
        sq.setAutoReverse(false);
        sq.play();

        this.mode = GameMode.AI;
        this.btnDoneText.setText("Play");
    }

    @FXML
    void btnSeatGamePressed() {
        btnSeatGame.setImage(new Image("uirenders/MenuBtnPressed.png"));
    }

    @FXML
    void btnSeatGameReleased() {
        btnSeatGame.setImage(new Image("uirenders/MenuBtnActive.png"));

        ScaleTransition st1 = new ScaleTransition(Duration.millis(500d), gridGamePropertiesParent);
        st1.setFromX(1d);
        st1.setFromY(1d);
        st1.setFromZ(1d);
        st1.setToX(0d);
        st1.setToZ(0d);
        st1.setToY(0d);
        st1.setCycleCount(1);
        st1.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                gridAiProperties.setVisible(false);
                gridAiProperties.setMouseTransparent(true);
                gridHotSeatProperties.setVisible(true);
                gridHotSeatProperties.setMouseTransparent(false);
                gridNetworkProperties.setVisible(false);
                gridNetworkProperties.setMouseTransparent(true);
            }
        });

        TranslateTransition tt1 = new TranslateTransition(Duration.millis(300d), polygonArrow);
        tt1.setFromY(polygonArrow.getTranslateY());
        tt1.setToY(95d);
        tt1.setCycleCount(1);
        tt1.setAutoReverse(false);

        ScaleTransition st2 = new ScaleTransition(Duration.millis(500d), gridGamePropertiesParent);
        st2.setFromX(0d);
        st2.setFromY(0d);
        st2.setFromZ(0d);
        st2.setToX(1d);
        st2.setToZ(1d);
        st2.setToY(1d);
        st2.setCycleCount(1);


        SequentialTransition sq =  new SequentialTransition();
        sq.getChildren().addAll(st1, tt1, st2);
        sq.setCycleCount(1);
        sq.setAutoReverse(false);
        sq.play();

        this.mode = GameMode.HOTSEAT;
        this.btnDoneText.setText("Play");
    }

    @FXML
    void btnNetGamePressed() {
        btnNetGame.setImage(new Image("uirenders/MenuBtnPressed.png"));
    }

    @FXML
    void btnNetGameReleased() {
        btnNetGame.setImage(new Image("uirenders/MenuBtnActive.png"));

        ScaleTransition st1 = new ScaleTransition(Duration.millis(500d), gridGamePropertiesParent);
        st1.setFromX(1d);
        st1.setFromY(1d);
        st1.setFromZ(1d);
        st1.setToX(0d);
        st1.setToZ(0d);
        st1.setToY(0d);
        st1.setCycleCount(1);
        st1.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                gridAiProperties.setVisible(false);
                gridAiProperties.setMouseTransparent(true);
                gridHotSeatProperties.setVisible(false);
                gridHotSeatProperties.setMouseTransparent(true);
                gridNetworkProperties.setVisible(true);
                gridNetworkProperties.setMouseTransparent(false);
            }
        });

        TranslateTransition tt1 = new TranslateTransition(Duration.millis(300d), polygonArrow);
        tt1.setFromY(polygonArrow.getTranslateY());
        tt1.setToY(280d);
        tt1.setCycleCount(1);
        tt1.setAutoReverse(false);

        ScaleTransition st2 = new ScaleTransition(Duration.millis(500d), gridGamePropertiesParent);
        st2.setFromX(0d);
        st2.setFromY(0d);
        st2.setFromZ(0d);
        st2.setToX(1d);
        st2.setToZ(1d);
        st2.setToY(1d);
        st2.setCycleCount(1);


        SequentialTransition sq =  new SequentialTransition();
        sq.getChildren().addAll(st1, tt1, st2);
        sq.setCycleCount(1);
        sq.setAutoReverse(false);
        sq.play();

        this.mode = GameMode.NETWORK;

        this.btnDoneText.setText("Ready");
    }

    @FXML
    void btnDiffEasyPressed() {

    }

    @FXML
    void btnDiffEasyReleased() {
        btnDiffEasy.setImage(new Image("uirenders/MenuBtnPressed.png"));
    }

    @FXML
    void btnDiffHardPressed() {

    }

    @FXML
    void btnDiffHardReleased() {

    }

    private Client gameClient = Client.getCurrent();

    @FXML
    void btnConnectMouseReleased() {
        if (chkboxServer.isSelected() && chkboxServer.isDisabled()){
            try{
                gameClient.disconnectFromServer();
                this.printMessage("Disconnected from local server.\n");
                this.stopLocalServer();
                this.disableBtnDone();
            } catch (IOException ex){
                this.printMessage("Couldn't disconnect from local server.\n");
                ex.printStackTrace();
            }
        }
        else if (chkboxServer.isSelected()) {
            this.startLocalServer(txtBoxIpPort.getText());
        }
        else if (gameClient.isConnectedToServer() == false) {
            String[] params = txtBoxIpPort.getText().split(":", -1);
            this.connectToServer(params[0], params[1]);
        }
        else {
            try{
                gameClient.disconnectFromServer();
                this.printMessage("Disconnected from server.\n");
                this.btnConnect.setText("Connect");
                this.disableBtnDone();
            } catch (IOException ex){
                this.printMessage("Couldn't disconnect from server. Disconnecting forcefully!\n");
                gameClient.forceDisconnectFromServer();
                ex.printStackTrace();
            }
        }
    }

    private StringBuilder messageBuilder = new StringBuilder();

    public synchronized void printMessage(String message){
        messageBuilder.append(message);
        txtAreaNetgame.setText(messageBuilder.toString());
        txtAreaNetgame.setScrollTop(Double.MAX_VALUE);
    }

    @FXML
    void networkInputOnKeyPressed(javafx.scene.input.KeyEvent event) {
        if (KeyCode.ENTER.equals(event.getCode())) {
            String command = txtBoxNetworkInput.getText();
            //this.printMessage("Parsing command " + command + "...\n");
            this.parseConsoleCommand(command);
            this.txtBoxNetworkInput.clear();
        }
    }

    @FXML
    void chkboxServerMouseReleased() {
        if (this.chkboxServer.isSelected()){
            this.btnConnect.setText("Start local server");
            this.txtIpPort.setText("Port: ");
            this.txtBoxIpPort.setPromptText("Example: 5555");
        }
        else {
            this.btnConnect.setText("Connect");
            this.txtIpPort.setText("IP and port: ");
            this.txtBoxIpPort.setPromptText("Example: 192.168.1.1:5555");
        }
    }

    private void startLocalServer(String port){
        Server server = Server.getCurrent();

        try {
            server.setPort(Integer.parseInt(port));

            server.startServer();
            this.connectToServer("127.0.0.1", port);
            this.printMessage("Server started.\n");
            this.btnConnect.setText("Stop server");
            this.chkboxServer.setDisable(true);
            this.enableBtnDone();

        } catch (NumberFormatException ex) {
            this.printMessage("Invalid port.\n");
            ex.printStackTrace();
        } catch (IOException e){
            this.printMessage("Server could not be started.\n");
            e.printStackTrace();
        }
    }

    private void stopLocalServer(){
        Server server = Server.getCurrent();

        try{
            server.stopServer();
            this.printMessage("Server stopped.\n");
            this.btnConnect.setText("Start server");
            this.chkboxServer.setDisable(false);
        }
        catch (IOException ex){
            this.printMessage("Server could not be stopped.\n");
            ex.printStackTrace();
        }
    }

    private void connectToServer(String ip, String port){
        if (ip == null && port == null){
            this.printMessage("Wrong parameters for connection. Enter IP and port.\n");
        }
        else if (ip.equals("") || port.equals("")){
            this.printMessage("Wrong parameters for connection. Enter IP and port.\n");
        }
        else{
            try {
                gameClient.connectToServer(ip, port, playerOne);
                this.printMessage("Connected to server.\n");
                this.btnConnect.setText("Disconnect");
                this.enableBtnDone();
            } catch(NumberFormatException e) {
                this.printMessage("Port not valid. Enter valid port integer.\n");
                e.printStackTrace();
            } catch (IOException ex) {
                this.printMessage("Server not responding.\n");
                ex.printStackTrace();
            }
        }
    }

    public void disconnectedFromServer(){
        this.printMessage("Server disconnected you.\n");
        this.btnConnect.setText("Connect");
        this.chkboxServer.setDisable(false);
        this.chkboxServer.setSelected(false);
        disableBtnDone();
    }

    private void parseConsoleCommand(String command){
        String[] fullCommand = new String[6];
        fullCommand = command.split(" ", -1);

        if(fullCommand[0] != null){
            switch(fullCommand[0].toUpperCase()){
                case "CONNECT":
                    this.connectToServer(fullCommand[1], fullCommand[2]);
                    break;
                case "STARTS":
                    if (fullCommand[1] != null) {
                        this.startLocalServer(fullCommand[1]);
                    }
                    else{

                    }

                    break;
                case "PING":
                    if (this.gameClient.isConnectedToServer()) {
                        try {
                            gameClient.sendCustomCommand("PING");
                            this.printMessage("Ping sent.\n");
                        } catch (IOException ex){
                            this.printMessage("Ping couldn't be sent.\n");
                            ex.printStackTrace();
                        }
                    }
                    else{
                        this.printMessage("Not connected to any server.\n");
                    }
                    break;
                case "CMD":
                    if (this.gameClient.isConnectedToServer()) {
                        try {
                            gameClient.sendCustomCommand(command.toUpperCase());
                            this.printMessage("Command sent.\n");
                        } catch (IOException ex){
                            this.printMessage("Command couldn't be sent.\n");
                            ex.printStackTrace();
                        }
                    }
                    else{
                        this.printMessage("Not connected to any server.\n");
                    }
                    break;
                case "CLEAR":
                    messageBuilder = new StringBuilder();
                    txtAreaNetgame.setText(messageBuilder.toString());
                    break;
                default:
                    if (this.gameClient.isConnectedToServer()){
                        try{
                            gameClient.sendCustomCommand("CHAT " + command.replace(' ', '&'));
                            //this.printMessage(": " + command + "\n");
                        } catch (IOException ex){
                            this.printMessage("Message couldn't be sent.\n");
                            ex.printStackTrace();
                        }
                    }
                    else {
                        this.printMessage("You're not connected to any server so you can't chat.\n");
                    }
                    break;
            }
        }
        else {
            this.printMessage("Couldn't parse input.\n");
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert txtAreaNetgame != null : "fx:id=\"txtAreaNetgame\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnChangeColorOne != null : "fx:id=\"btnChangeColorOne\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnConnect != null : "fx:id=\"btnConnect\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert txInPlayerTwo != null : "fx:id=\"txInPlayerTwo\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnDiffEasy != null : "fx:id=\"btnDiffEasy\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert mainGrid != null : "fx:id=\"mainGrid\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnDiffHardText != null : "fx:id=\"btnDiffHardText\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnAiGame != null : "fx:id=\"btnAiGame\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert bgImage != null : "fx:id=\"bgImage\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnDiffHard != null : "fx:id=\"btnDiffHard\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert nickLeft != null : "fx:id=\"nickLeft\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert txtBoxIpPort != null : "fx:id=\"txtBoxIpPort\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert nickRight != null : "fx:id=\"nickRight\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnSeatGame != null : "fx:id=\"btnSeatGame\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnPl2ColorText != null : "fx:id=\"btnPl2ColorText\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert textPlayerTwo != null : "fx:id=\"textPlayerTwo\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert txtBoxNetworkInput != null : "fx:id=\"txtBoxNetworkInput\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert textPlayerOne != null : "fx:id=\"textPlayerOne\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnNetGame != null : "fx:id=\"btnNetGame\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnChangeColorTwo != null : "fx:id=\"btnChangeColorTwo\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert txInPlayerOne != null : "fx:id=\"txInPlayerOne\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnDiffEasyText != null : "fx:id=\"btnDiffEasyText\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert txtIpPort != null : "fx:id=\"txtIpPort\" was not injected: check your FXML file 'GameSetup.fxml'.";
        assert btnDone != null : "fx:id=\"btnDone\" was not injected: check your FXML file 'GameSetup.fxml'.";

        sceneManager = SceneManager.getCurrent();

        Lighting lighting = new Lighting();
        lighting.setSurfaceScale(1.5);

        nickLeft.setEffect(lighting);
        nickRight.setEffect(lighting);

        playerOne.setColorString("WHITE");
        playerTwo.setColorString("WHITE");
        playerOne.setName(textPlayerOne.getText());
        playerTwo.setName(textPlayerTwo.getText());
        playerOne.setId(-1);
        playerTwo.setId(-2);

        this.disableBtnDone();

        FadeTransition ft = new FadeTransition(Duration.millis(1000d), mainGrid);
        ft.setToValue(1.0d);
        ft.setFromValue(0.0d);
        /*
        ScaleTransition st = new ScaleTransition(Duration.millis(500d), gridGamePropertiesParent);
        st.setFromX(0d);
        st.setFromY(0d);
        st.setFromZ(0d);
        st.setToX(1d);
        st.setToZ(1d);
        st.setToY(1d);
        st.setCycleCount(1);*/

        gridAiProperties.setVisible(true);
        gridAiProperties.setMouseTransparent(false);

        SequentialTransition sq =  new SequentialTransition();
        sq.getChildren().addAll(ft);
        sq.setCycleCount(1);
        sq.setAutoReverse(false);
        sq.play();

        gameClient.setControlledScreen(this);
    }

    public void gotoGameBoard() throws IOException {
        FadeTransition ft = new FadeTransition(Duration.millis(1000d), mainGrid);
        ft.setToValue(0.1d);
        ft.setFromValue(1.0d);
        ft.play();

        sceneManager.navigate("GameBoard.fxml");
    }
}
