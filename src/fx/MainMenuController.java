/**
 * Sample Skeleton for 'MainMenu.fxml' Controller Class
 */

package fx;

import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import objects.Player;
import system.Client;
import system.Server;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainMenuController extends SceneController implements Initializable {

    Stage prevStage;

    public void setPrevStage(Stage stage){
        this.prevStage = stage;
    }

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="mainGrid"
    private GridPane mainGrid; // Value injected by FXMLLoader

    @FXML
    private ImageView bgImage;

    @FXML // fx:id="newGameButton"
    private ImageView newGameButton; // Value injected by FXMLLoader

    @FXML
    private ImageView exitGameButton;

    @FXML
    private ImageView loadGameButton;

    @FXML
    private GridPane console;

    @FXML
    private TextArea consoleOut;

    @FXML
    private TextField consoleIn;

    private StringBuilder consoleOutBuilder = new StringBuilder();

    private Client gameClient;

    @FXML
    void newGameButtonClicked(javafx.scene.input.MouseEvent event) {
        newGameButton.setImage(new Image("uirenders/MenuBtnPressed.png"));
    }

    @FXML
    void newGameButtonReleased() throws IOException {
        newGameButton.setImage(new Image("uirenders/MenuBtnActive.png"));
        this.gotoGameSetup();
    }

    @FXML
    void loadGameButtonClicked() {

    }

    @FXML
    void loadGameButtonReleased() {

    }

    @FXML
    void exitGameButtonClicked() {
        exitGameButton.setImage(new Image("uirenders/MenuBtnPressed.png"));
    }

    @FXML
    void exitGameButtonReleased() {
        exitGameButton.setImage(new Image("uirenders/MenuBtnActive.png"));
        System.exit(0);
    }

    @FXML
    void consoleOnKeyPressed(javafx.scene.input.KeyEvent event) {
        if (KeyCode.CAPS.equals(event.getCode())) {
            this.toggleConsole();
        }
    }

    @FXML
    void consoleInOnKeyPressed(javafx.scene.input.KeyEvent event) {
        if (KeyCode.ENTER.equals(event.getCode())) {
            String command = consoleIn.getText();
            this.displayConsoleMessage("Parsing command " + command + "...\n");
            this.parseConsoleCommand(command);
            //this.consoleIn.setText("");
            this.consoleIn.clear();
        }
    }

    private void toggleConsole(){
        TranslateTransition tt1 = new TranslateTransition(Duration.millis(200d), console);
        tt1.setCycleCount(1);
        tt1.setAutoReverse(false);

        if (console.getTranslateY() < 0){
            tt1.setFromY(-270);
            tt1.setToY(0d);
        }
        else{
            tt1.setFromY(0d);
            tt1.setToY(-270d);
        }

        tt1.play();
    }

    private void parseConsoleCommand(String command){
        String[] fullCommand = new String[6];
        fullCommand = command.split(" ", -1);

        if(fullCommand[0] != null){
            switch(fullCommand[0].toUpperCase()){
                case "CONNECT":
                    if (fullCommand[1] != null && fullCommand[2] != null){
                        this.gameClient = Client.getCurrent();
                        gameClient.setControlledScreen(this);
                        try {
                            gameClient.connectToServer(fullCommand[1], fullCommand[2], new Player(-1, "debug", Color.WHITE, 12));
                            this.displayConsoleMessage("Connected to specified server.\n");
                        } catch(Exception e) {
                            this.displayConsoleMessage("Port not valid. Enter valid port integer.\n");
                            e.printStackTrace();
                        }
                    }
                    else{
                        this.displayConsoleMessage("Wrong parameters for connection. Enter IP and port.\n");
                    }
                    break;
                case "STARTS":
                    if (fullCommand[1] != null) {
                        Server server = Server.getCurrent(); // TODO: quit

                        try {
                            server.setPort(Integer.parseInt(fullCommand[1]));
                        } catch(Exception e) {
                            this.displayConsoleMessage("Port not valid. Enter valid port integer.\n");
                            e.printStackTrace();
                        }
                        try{
                            server.startServer();
                            this.displayConsoleMessage("Server started.\n");
                        }
                        catch (Exception e){
                            this.displayConsoleMessage("Server could not be started.\n");
                            e.printStackTrace();
                        }
                    }
                    else{

                    }

                    break;
                case "PING":
                    if (this.gameClient != null) {
                        try {
                            gameClient.sendCustomCommand("PING");
                            this.displayConsoleMessage("Ping sent.\n");
                        } catch (IOException ex){
                            this.displayConsoleMessage("Ping couldn't be sent.\n");
                            ex.printStackTrace();
                        }
                    }
                    else{
                        this.displayConsoleMessage("Not connected to any server.\n");
                    }
                    break;
                case "CMD":
                    if (this.gameClient != null) {
                        try {
                            gameClient.sendCustomCommand(command.toUpperCase());
                            this.displayConsoleMessage("Command sent.\n");
                        } catch (IOException ex){
                            this.displayConsoleMessage("Command couldn't be sent.\n");
                            ex.printStackTrace();
                        }
                    }
                    else{
                        this.displayConsoleMessage("Not connected to any server.\n");
                    }
                    break;
                case "GAMEBOARD":
                    try {
                        sceneManager.navigate("GameBoard.fxml");
                    } catch (IOException ex) {
                        System.out.println("Couldn't navigate!");
                        ex.printStackTrace();
                    }
                    break;
                case "CLEAR":
                    consoleOutBuilder = new StringBuilder();
                    consoleOut.setText(consoleOutBuilder.toString());
                    break;
                default:
                    this.displayConsoleMessage("Unknown command. Refer to project's wiki.\n");
                    break;
            }
        }
        else {
            this.displayConsoleMessage("Couldn't parse input.\n");
        }
    }

    public void displayConsoleMessage(String message){
        consoleOutBuilder.append(message);
        consoleOut.setText(consoleOutBuilder.toString());
        consoleOut.setScrollTop(Double.MAX_VALUE);
    }

    @Override
    @FXML // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL location, ResourceBundle resources) {
        assert exitGameButton != null : "fx:id=\"exitGameButton\" was not injected: check your FXML file 'MainMenu.fxml'.";
        assert loadGameButton != null : "fx:id=\"loadGameButton\" was not injected: check your FXML file 'MainMenu.fxml'.";
        assert mainGrid != null : "fx:id=\"mainGrid\" was not injected: check your FXML file 'MainMenu.fxml'.";
        assert newGameButton != null : "fx:id=\"newGameButton\" was not injected: check your FXML file 'MainMenu.fxml'.";
        assert console != null : "fx:id=\"console\" was not injected: check your FXML file 'MainMenu.fxml'.";
        assert consoleOut != null : "fx:id=\"consoleOut\" was not injected: check your FXML file 'MainMenu.fxml'.";
        assert consoleIn != null : "fx:id=\"consoleIn\" was not injected: check your FXML file 'MainMenu.fxml'.";

        sceneManager = SceneManager.getCurrent();
        FadeTransition ft = new FadeTransition(Duration.millis(1000d), mainGrid);
        ft.setToValue(1.0d);
        ft.setFromValue(0.0d);
        ft.play();
    }

    private void gotoGameBoard() throws IOException {
        /*
        Stage stage = new Stage();
        stage.setTitle("JRisk! Game");
        Pane mainPane = null;
        mainPane = FXMLLoader.load(getClass().getResource("GameBoard.fxml"));
        Scene scene = new Scene(mainPane);
        stage.setScene(scene);
        prevStage.close();
        stage.show();
        */
        FadeTransition ft = new FadeTransition(Duration.millis(1000d), mainGrid);
        ft.setToValue(0.1d);
        ft.setFromValue(1.0d);
        ft.play();

        sceneManager.navigate("GameBoard.fxml");
    }

    private void gotoGameSetup() throws IOException {
        FadeTransition ft = new FadeTransition(Duration.millis(1000d), mainGrid);
        ft.setToValue(0.1d);
        ft.setFromValue(1.0d);
        ft.play();

        sceneManager.navigate("GameSetup.fxml");
    }
}
