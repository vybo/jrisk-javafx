package system;

import objects.*;

import java.io.DataOutputStream;
import java.util.*;

/**
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class Game {
    private Board board;
    private ArrayList<TerritoryCard> cardDeck;
    private ArrayList<GameGoal> goals;
    private HashMap<Integer, Player> players;
    private ArrayList<Integer> playerFront;
    private int playerIdOnStage = -1;
    private Stage gameStage;
    private String name;
    private Date startTime;
    private int delay;
    private boolean setupDone = false;

    /**
     * Konstruktor pro server
     */
    public Game(String name, int delay) {
        this.board = new Board();
        this.cardDeck = new ArrayList<TerritoryCard>();
        this.goals = new ArrayList<GameGoal>();
        this.players = new HashMap<Integer, Player>();
        this.gameStage = Stage.SETUP;
        this.name = name;
        this.delay = delay;
        this.playerFront = new ArrayList<Integer>();
    }

    /**
     * Konstruktor pro klienta
     */

    public Game(){
        this.board = new Board();
        this.cardDeck = new ArrayList<TerritoryCard>();
        this.goals = new ArrayList<GameGoal>();
        this.players = new HashMap<Integer, Player>();
        this.gameStage = Stage.SETUP;
        this.name = "";
        this.delay = 0;
        this.playerFront = new ArrayList<Integer>();
    }

    public void addPlayer(Player newPlayer){
        this.players.put(newPlayer.getId(), newPlayer);
    }

    public void generatePlayerFront(){
        Player[] allPlayers = players.values().toArray(new Player[players.size()]);

        for (Player pl : allPlayers) {
            playerFront.add(pl.getId());
        }
    }

    //TODO: Exception for no players
    public void nextPlayerToStage(){
        if (playerFront.get(playerFront.size() - 1) ==  playerIdOnStage){
            playerIdOnStage = playerFront.get(0);
            this.setupDone = true;
        }
        else if (playerIdOnStage == -1){
            playerIdOnStage = playerFront.get(0);
        }
        else {
            playerIdOnStage = playerFront.get(playerFront.indexOf(playerIdOnStage) + 1);
        }
    }

    public boolean isSetupDone() {
        return setupDone;
    }

    public int getPlayerIdOnStage(){
        return playerIdOnStage;
    }

    public void setPlayerIdOnStage(int newPlayerIdOnStage){
        this.playerIdOnStage = newPlayerIdOnStage;
    }

    public Player getPlayer(int id){
        return this.players.get(id);
    }

    public void removePlayer(int id){
        this.players.remove(id);
    }

    public boolean hasPlayer(int id){
        return this.players.containsKey(id);
    }

    /**
     * Navraceni vsech hracu v nemodifikovatelne kolekci
     * @return
     */
    public Collection<Player> getPlayers(){
        return Collections.unmodifiableCollection(players.values());
    }

    public Player getFirstPlayer (){
        Player[] allPlayers = players.values().toArray(new Player[players.size()]);

        return allPlayers[0];
    }

    public Player getSecondPlayer (){
        Player[] allPlayers = players.values().toArray(new Player[players.size()]);

        return allPlayers[1];
    }

    public String getName(){
        return this.name;
    }

    public Stage getGameStage (){
        return this.gameStage;
    }

    public void setGameStage(Stage newStage){
        this.gameStage = newStage;
    }

}
