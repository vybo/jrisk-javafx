package system;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class CommandThread implements Runnable {
    private static DataInputStream in;
    private static DataOutputStream out;

    private String command;

    public CommandThread(String command, DataInputStream in, DataOutputStream out){
        this.in = in;
        this.out = out;
        this.command = command;
    }

    @Override
    public void run() {
        try{
            this.out.writeUTF(command);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
