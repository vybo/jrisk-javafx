package system;

import javafx.scene.paint.Color;
import objects.Player;
import objects.Stage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Hlavni serverova trida implementujici obsluhu vlaken a herni logiku
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class Server {
    private static Server current;

    private Game game;
    private static ServerSocket serverSocket;
    private DataOutputStream out;
    private DataInputStream in;
    private int port;
    private ArrayList<ServerThread> clientThreads;
    private ConnectionThread newConnection;

    /*
    Privatni konstruktor kvuli jedinackovi
     */
    private Server() {
        /*
        Random generator = new Random();
        Player newPlayer = new Player(generator.nextInt(10) + 20, "ServerDefault", Color.YELLOW, 12);
        while (this.game.hasPlayer(newPlayer.getId())){
            newPlayer.setId(generator.nextInt(10) + 20);
        }
        this.game.addPlayer(newPlayer);
        */
    }

    /*
    Vytazeni/vytvoreni instance jedinacka
     */
    public static Server getCurrent() {
        //Je-li promenna instance null, tak se vytvori objekt
        if (current == null) {
            current = new Server();
        }
        //Vratime jedinacka
        return current;
    }

    /*
    Zalozeni serveru
     */
    public void startServer() throws IOException {
        this.game = new Game();
        this.clientThreads = new ArrayList<ServerThread>();

        System.out.println("Starting game server...");

        /*
        Spusteni vlakna, ktere bude cekat na pripojeni neomezeneho mnozstvi klientu.
         */
        try{
            serverSocket = new ServerSocket(port);
            newConnection = new ConnectionThread(serverSocket, this);
            Thread thread = new Thread(newConnection);
            thread.start();
            System.out.println("Server running and waiting for connection...");

        } catch(Exception e){
            System.out.println("Could not start server - binding error");
            e.printStackTrace();
            System.exit(1);
        }
    }

    /*
    Metoda volana vlaknem cekajici na pripojeni klientu. Vytvori nove vlakno, ktere zpracovava prikazy od jednoho klienta.
     */
    public void connectUser(DataInputStream in, DataOutputStream out, Socket socket){
        ServerThread user = new ServerThread(out, in, this, socket);
        Thread thread = new Thread(user);
        thread.start();

        // Seznam vsech vlaken obsluhujicich vsechny klienty
        this.clientThreads.add(user);
    }

    public boolean canConnect(){
        if (game.getPlayers().size() < 3){
            return true;
        }
        else {
            return false;
        }
    }

    /*
     Metoda volana vlaknem obsluhujicim klienta, preda zpravu vsem ostatnim vlaknum - klientum
     */
    public void sendCommandToAllClients(String cmd){
        //int callerId = caller.getMyId();

        for (ServerThread connectedThread : clientThreads){
            try {
                    connectedThread.sendCommand(cmd);
            } catch (IOException ex){
                System.out.println("Couldn't send command: " + cmd + " to thread.");
                ex.printStackTrace();
            }
        }
    }

    /*
    Metoda uklizejici vlakna po odpojeni jejich klientu
     */
    public synchronized void disconnectThread(ServerThread sender){
        this.clientThreads.remove(sender);
        game.removePlayer(sender.getMyId());
    }

    /**
     * Metoda, ktera je volana ServerThreadem potom, co se pripoji novy klient a potrebuje prijmout vsechny ostatni hrace
     * @param senderId
     */
    public void sendAllOtherPlayers(ServerThread caller){
        for (Player pl : game.getPlayers()){
            if (caller.getMyId() != pl.getId()){
                try{
                    caller.sendCommand("CON " + pl.getId() + " " + pl.getName() + " " + pl.getColorString());
                } catch (IOException ex) {
                    System.out.println("Couldn't send connected players to new thread id: " + caller.getMyId());
                    ex.printStackTrace();
                }

            }
        }
    }

    /*
    Metoda prepinajici readiness hrace
     */
    public void switchReadyPlayer(int id){
        Player pl = game.getPlayer(id);
        if (pl.getReady()){
            pl.setReady(false);
        } else {
            pl.setReady(true);
        }
    }
 /* TODO: Dodelat cancelling odpocitavani
    Runnable ticker = new Runnable() {
        private boolean cancelled = false;

        @Override
        public void run() {
            tick();
        }

        public void cancel() {
            this.cancelled = true;
        }

        public void reset(){
            this.cancelled = false;
        }

        private void tick() {
            try {
                int i = 5;
                while (!cancelled && i > 0) {
                    sendCommandToAllClients("TELL " + ("Game commending in " + i + "seconds.").replace(' ', '&'));
                    Thread.sleep(1000);
                    i--;
                }

                if (!cancelled)
                    commenceGame();
            } catch (InterruptedException ex) {
            }
        }
    };*/

    public void checkAllReadiness(){
        boolean readiness = true;

        for (Player pl : game.getPlayers()){
            if (!pl.getReady())
                readiness = false;
        }

        if (readiness){
            commenceGame();
            /*
            ticker.run();
        } else {
            ticker.cancel();*/
        }
    }

    public void commenceGame(){
        game.setGameStage(Stage.SETUP);
        game.generatePlayerFront();
        sendCommandToAllClients("START");
        tick();
    }

    public void tick(){
        Stage currentStage = this.game.getGameStage();
        switch(currentStage) {
            case SETUP:
                if (game.isSetupDone()){
                    game.setGameStage(Stage.DRAFT);
                    game.nextPlayerToStage();
                    sendCommandToAllClients("STAGE DRAFT" + " " + game.getPlayerIdOnStage());
                }
                else {
                    game.nextPlayerToStage();
                    sendCommandToAllClients("STAGE SETUP" + " " + game.getPlayerIdOnStage());
                }
                break;
            case DRAFT:
                game.setGameStage(Stage.ATTACK);
                sendCommandToAllClients("STAGE ATTACK" + " " + game.getPlayerIdOnStage());
                break;
            case ATTACK:
                game.setGameStage(Stage.FORTIFY);
                sendCommandToAllClients("STAGE FORTIFY" + " " + game.getPlayerIdOnStage());
                break;
            case FORTIFY:
                game.nextPlayerToStage();
                game.setGameStage(Stage.DRAFT);
                sendCommandToAllClients("STAGE DRAFT" + " " + game.getPlayerIdOnStage());
                break;
            default:
                break;
        }
    }

    /*
    Zastaveni serveru a odpojeni vsech klientu.
     */
    public synchronized void stopServer() throws IOException{
        for (ServerThread connectedThread : clientThreads){
            connectedThread.exitGame("8");
        }
        clientThreads.clear();
        serverSocket.close();
        serverSocket = null;
    }

    /*
    Nastaveni portu naslouchajicim pripojeni
     */
    public void setPort(int port){
        this.port = port;
    }

    /*
    Getter na hlavni herni objekt
     */
    public Game getGame(){
        return this.game;
    }
}
