package system;

import fx.GameSetupController;
import fx.MainMenuController;
import fx.SceneController;
import javafx.scene.paint.Color;
import objects.Player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

/**
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class ClientThread implements Runnable {
    private Client client;
    private static DataInputStream in;
    private static DataOutputStream out;
    String command;

    public ClientThread(Client client, DataInputStream in, DataOutputStream out){
        this.client = client;
        this.in = in;
        this.out = out;
        command = "";
    }

    /*
    Hlavni smycka, ktera cte a zpracovava zpravy od serveru a vykonava prislusne akce.
    Vetsinou to jsou volani do hlavni tridy klienta, ten potom vola udalosti UI.
     */
    @Override
    public void run() {
        String[] fullCommand = new String[6];
        Game game = client.getGame();
        //SceneController currentController = client.getUiController();

        while(command != "END"){
            try {
                command = in.readUTF();
                fullCommand = command.split(" ", -1);

                if(fullCommand[0] != null){
                    switch(fullCommand[0]){
                        case "PING":
                            client.sendMessageToController("Server responded with PING\n");
                            break;
                        case "ACON":
                            client.sendMessageToController("Successfully connected to server.\n");
                            client.sendMessageToController("Your id: " + fullCommand[1] + "\n");
                            this.out.writeUTF("AREC ACON");
                            break;
                        case "DCON":
                            client.sendMessageToController("Server declined the connection. Reason: " + fullCommand[1] + "\n");
                            client.forceDisconnectFromServer();
                            return;
                            //break;
                        case "STAGE":
                            client.setGameStage(fullCommand[1], Integer.parseInt(fullCommand[2]));
                            break;
                        case "CHAT":
                            client.sendMessageToController(game.getPlayer(Integer.parseInt(fullCommand[1])).getName() + " says: " + translateToReadable(fullCommand[2]) + "\n");
                            this.out.writeUTF("AREC CHAT");
                            break;
                        case "TELL":
                            client.sendMessageToController("Server : " + translateToReadable(fullCommand[1]) + "\n");
                            this.out.writeUTF("AREC TELL");
                            break;
                        case "CON":
                            Player newPlayer = new Player(Integer.parseInt(fullCommand[1]), fullCommand[2], Color.WHITE, 12);
                            newPlayer.setColorString(fullCommand[3]);
                            game.addPlayer(newPlayer);
                            client.sendMessageToController("Player " + newPlayer.getName() + " connected with id " + newPlayer.getId() + " and color " + newPlayer.getColorString() + "\n");
                            this.out.writeUTF("AREC CON");
                            break;
                        case "NCON":
                            Player disc = game.getPlayer(Integer.parseInt(fullCommand[1]));
                            game.removePlayer(disc.getId());
                            client.sendMessageToController("Player " + disc.getName() + " disconnected. Reason: " + fullCommand[2] + ".\n");
                            this.out.writeUTF("AREC NCON");
                            break;
                        case "START":
                            client.startGame();
                            break;
                        case "ATTACK":
                            client.attackOccured(Integer.parseInt(fullCommand[1]), Integer.parseInt(fullCommand[2]));
                            break;
                        case "AREC":


                            break;
                        case "DREC":
                            client.sendMessageToController("Server declined the command. Reason: " + fullCommand[1] + "\n");
                            break;
                        case "END":
                            client.sendMessageToController("Game ended. Reason: " + fullCommand[1] + "\n");
                            client.disconnectedFromServer();
                            return;
                            //break;
                        default:
                            client.sendMessageToController("Unknown server response\n");
                            break;
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /*
    Prevedeni zpravy z bezparametrove podoby, jelikoz komunikcni parametry
    jsou oddeleny mezerami.
     */
    private String translateToReadable(String what){
        return what.replace('&', ' ');
    }

    /*
    Manualni odpojeni se ze hry a zaroven vzdani se.
     */
    public synchronized void exitGame(String reason) throws IOException{
        this.command = "END";
        this.out.writeUTF("END " + reason);
    }

    /*
    Univerzalni poslani prikazu na server.
     */
    public void sendCommand(String cmd) throws IOException {
        this.out.writeUTF(cmd);
    }
}
