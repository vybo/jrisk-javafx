package system;

import javafx.scene.paint.Color;
import objects.Player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Trida ServerThread umoznujici uzivatelum pracovat se serverem
 *
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class ServerThread implements Runnable {
    private DataOutputStream out;
    private DataInputStream in;
    private Socket socket;
    private Server server;
    private HashMap<String, String[]> oldCommands;
    private String command;
    private int myId;

    public ServerThread(DataOutputStream out, DataInputStream in, Server server, Socket socket){
        this.out = out;
        this.in = in;
        this.server = server;
        this.socket = socket;
        this.oldCommands = new HashMap<String, String[]>();
        this.command = "";
    }

    /*
    Hlavni naslouchajici a reagujici smycka. Cte prikazy od klienta, zpracuje je a provede prislusnou akci.
    Prislusna akce = vetsinou volani herni logiky hlavniho serveru
     */
    @Override
    public void run() {

        String[] fullCommand = new String[6];
        Game game = server.getGame();

        while(!command.equals("END")){
            try{
                command = in.readUTF();
                fullCommand = command.split(" ", -1);

            } catch(Exception e) {
                e.printStackTrace();
            }
            if(fullCommand[0] != null){
                /*try{
                    if (oldCommands.size() != 0 && fullCommand[0].equals("AREC") == false){
                        this.out.writeUTF("DREC 100");
                        fullCommand[0] = "NULL";
                    }
                    else if (oldCommands.size() != 0 && fullCommand[0].equals("AREC") == true){
                        oldCommands.clear();
                        fullCommand[0] = "NULL";
                    }
                } catch (IOException ex){
                    return;
                }*/

                switch(fullCommand[0]){
                    case "CON":
                        try {
                            Random generator = new Random();
                            Player newPlayer = new Player(generator.nextInt(10) + 1, fullCommand[1], Color.AQUA, 12);
                            newPlayer.setColorString(fullCommand[2]);
                            myId = newPlayer.getId();
                            game.addPlayer(newPlayer);

                            if (server.canConnect()){
                                System.out.println("Player with id " + newPlayer.getId() + ", name " + newPlayer.getName() + ", color " + newPlayer.getColorString() + " connected.");
                                this.out.writeUTF("ACON " + newPlayer.getId());
                                //this.oldCommands.put("CON" , fullCommand);
                                server.sendCommandToAllClients(("CON " + newPlayer.getId() + " " + newPlayer.getName() + " " + newPlayer.getColorString())); // odeslat vsem oznameni o pripojeni
                                server.sendAllOtherPlayers(this);
                            }
                            else {
                                System.out.println("Denied connection to player. Server full.");
                                this.out.writeUTF("DCON 1");
                                server.disconnectThread(this);
                                return;
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            return;
                        }
                        break;
                    case "PING":
                        try {
                            System.out.println("Thread received ping, pinging back.");
                            this.out.writeUTF("PING");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            return;
                        }
                        break;
                    case "DONE":
                        System.out.println("Done received, advancing game stage...");
                        server.tick();
                        break;
                    case "CHAT":
                        System.out.println(game.getPlayer(myId).getName() + ": " + fullCommand[1]);
                        server.sendCommandToAllClients(("CHAT " + myId + " " +fullCommand[1])); // odeslani vsem klientum chat zpravu
                        //this.oldCommands.put("CHAT", )
                        break;
                    case "READY":
                        server.switchReadyPlayer(myId);

                        if (game.getPlayer(myId).getReady()){
                            System.out.println(game.getPlayer(myId).getName() + " is ready.");
                            server.sendCommandToAllClients(("TELL " + ("Player " + game.getPlayer(myId).getName() + " is ready.").replace(' ', '&'))); // odeslani vsem klientum zpravu
                        }
                        else {
                            System.out.println(game.getPlayer(myId).getName() + " is not ready.");
                            server.sendCommandToAllClients(("TELL " + ("Player " + game.getPlayer(myId).getName() + " is not ready.").replace(' ', '&'))); // odeslani vsem klientum chat zpravu
                        }

                        server.checkAllReadiness();
                        break;
                    case "ATTACK":
                        server.sendCommandToAllClients(command); // TODO: make this better
                        /*
                        try {

                        } catch (IOException ex) {
                            return;
                        }*/
                        break;
                    /*
                    case "PLACE":
                        try {

                        } catch (IOException ex) {
                            return;
                        }
                        break;
                    case "FORTIFY":
                        try {

                        } catch (IOException ex) {
                            return;
                        }
                        break;
                    case "DREC":
                        try {

                        } catch (IOException ex) {
                            return;
                        }
                        break;
                        */
                    case "NULL":
                        break;
                    case "END":
                        System.out.println("Client id: " + myId + " disconnected. Reason: " + fullCommand[1]);
                        server.sendCommandToAllClients(("NCON " + myId + " " + "8")); // oznameni ostatnim klientum o mem odpojeni
                        server.disconnectThread(this);
                        return;
                        //break;
                    case "AREC":
                        System.out.println("Confirmation of receiving command " + fullCommand[1] + " received.");
                        break;
                    default:
                        try {
                            this.out.writeUTF("DREC 99");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        break;
                }
            }
        }
    }

    /*
    Metoda oznamujici ukonceni hry klienta
     */
    public void exitGame(String reason) throws IOException{
        this.command = "END";
        this.out.writeUTF("END " + reason);
    }

    /*
    Univerzalni odeslani jakehokoliv prikazu
     */
    public  void sendCommand(String cmd) throws IOException{
        this.out.writeUTF(cmd);
    }

    public int getMyId() { return myId; }
}

