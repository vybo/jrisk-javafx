package system;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by danvybiral on 18/11/14.
 */
public class ConnectionThread implements Runnable {
    private static ServerSocket serverSocket;
    private Server server;


    public ConnectionThread(ServerSocket serverSocket, Server server){
        this.serverSocket = serverSocket;
        this.server = server;
    }

    @Override
    public void run() {
        try{
            this.listen();
        } catch (Exception e) {
            System.out.println("Can't wait for connection or server forcefully stopped.");
            e.printStackTrace();
        }
    }

    private void listen() throws IOException, InterruptedException{
        while (true){
            Socket socket = serverSocket.accept();
            System.out.println("Client connected. IP: " + socket.getInetAddress());

            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            DataInputStream in = new DataInputStream(socket.getInputStream());

            this.server.connectUser(in, out, socket);
        }
    }
}
