package system;

import fx.GameBoardController;
import fx.GameSetupController;
import fx.MainMenuController;
import fx.SceneController;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import objects.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author Dan Vybiral <dan.vybiral@mendelu.cz>
 */
public class Client {
    private static Client current;

    private SceneController uiController;
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private Game game;
    //private Player player;
    private ClientThread clientThread;
    private int myId;

    private Client() {
        this.game = new Game();
        //this.player = new Player();
    }

    public static Client getCurrent() {
        //Je-li promenna instance null, tak se vytvori objekt
        if (current == null) {
            current = new Client();
        }

        //Vratime jedinacka
        return current;
    }

    public void setControlledScreen(SceneController controller){
        this.uiController = controller;
    }

    public void connectToServer(String host, String port, Player player) throws IOException {
        this.game.addPlayer(player);

        //Zkouska pripojeni k serveru
        this.socket = new Socket(host, Integer.parseInt(port));

        //Pokud se pripojeni povedlo, zacinam komunikovat
        this.out = new DataOutputStream(socket.getOutputStream());
        this.in = new DataInputStream(socket.getInputStream());

        //Pozadavek na pripojeni s aktualne nastavenym hracem
        //Odeslani prikazu na server
        this.out.writeUTF("CON " + player.getName() + " " + player.getColorString()); // TODO: Jinak poslat barvu

        //Kontrola odpovedi
        String reply = in.readUTF();
        String[] fullReply = new String[6];
        fullReply = reply.split(" ", -1);

        if (fullReply[0].equals("ACON")){
            Player tmp = game.getPlayer(-1);
            game.removePlayer(-1);
            tmp.setId(Integer.parseInt(fullReply[1])); // Nastaveni id mojeho hrace na id pridelenym serverem
            game.addPlayer(tmp);
            this.myId = Integer.parseInt(fullReply[1]);
            //this.player.setId(Integer.parseInt(fullReply[1]));
            out.writeUTF("AREC ACON"); // Zprava serveru o tom, ze se klient uspesne pripojil a prijal data

            //Start uzivatelskeho vlakna na prijimani zprav ze serveru
            clientThread = new ClientThread(this, in, out);
            Thread thread = new Thread(clientThread);
            thread.start();

            this.sendMessageToController("Successfully connected to server.\n");
            this.sendMessageToController("Your id: " + fullReply[1] + "\n");
        }
        else if (fullReply[0].equals("DCON")){
            this.sendMessageToController("Server declined the connection. Reason: " + fullReply[1] + "\n");
        }
        else {
            this.sendMessageToController("Error connecting to the server. Unknown reason.\n");
        }
    }

    public void sendMessageToController(String message){
        if (uiController instanceof MainMenuController){
            ((MainMenuController) uiController).displayConsoleMessage(message);
        }
        else if (uiController instanceof GameSetupController){
            ((GameSetupController) uiController).printMessage(message);
        }
    }

    public void disconnectFromServer() throws IOException{
        this.clientThread.exitGame("8");
        this.clientThread = null;
        this.socket.close();
        this.socket = null;
    }

    public void stageDone() throws IOException{
        sendCustomCommand("DONE");
    }

    public void sendAttack(int territoryIdFrom, int territoryIdTo) {
        // Needs game.attack
        try {
            sendCustomCommand("ATTACK " + territoryIdFrom + " " + territoryIdTo);
        } catch (IOException e) {
            System.out.println("Couldn't attack.");
            e.printStackTrace();
        }
    }

    public void attackOccured(int territoryIdFrom, int territoryIdTo){
        if (uiController instanceof GameBoardController){
            ((GameBoardController) uiController).attackOccured(territoryIdFrom, territoryIdTo);
        }
    }

    public void setGameStage(String toStage, int newPlayerIdOnStage){
        game.setPlayerIdOnStage(newPlayerIdOnStage);
        switch (toStage){
            case "SETUP":
                game.setGameStage(Stage.SETUP);
                if (uiController instanceof GameBoardController){
                ((GameBoardController) uiController).changeGameStage(Stage.SETUP, newPlayerIdOnStage);
                }
                break;
            case "ATTACK":
                game.setGameStage(Stage.ATTACK);
                if (uiController instanceof GameBoardController){
                    ((GameBoardController) uiController).changeGameStage(Stage.ATTACK, newPlayerIdOnStage);
                }
                break;
            case "DRAFT":
                game.setGameStage(Stage.DRAFT);
                if (uiController instanceof GameBoardController){
                    ((GameBoardController) uiController).changeGameStage(Stage.DRAFT, newPlayerIdOnStage);
                }
                break;
            case "FORTIFY":
                game.setGameStage(Stage.FORTIFY);
                if (uiController instanceof GameBoardController){
                    ((GameBoardController) uiController).changeGameStage(Stage.FORTIFY, newPlayerIdOnStage);
                }
                break;
            default:

                break;
        }
    }

    public synchronized void forceDisconnectFromServer() {
        try {
            this.clientThread.exitGame("8");
            this.socket.close();

            if (uiController instanceof GameSetupController){
                ((GameSetupController) uiController).disconnectedFromServer();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.clientThread = null;
        this.socket = null;
    }

    public void disconnectedFromServer() throws IOException{
        this.clientThread = null;
        this.socket.close();
        this.socket = null;

        if (uiController instanceof GameSetupController){
            ((GameSetupController) uiController).disconnectedFromServer();
        }
    }

    public void playerReady() throws IOException{
        sendCustomCommand("READY " + myId); // Neni duvod posilat moje id, nesouhlasi s protokolem
        /*
        if (this.socket != null && this.out != null && this.in != null){
            this.out.writeUTF("READY");

            //Kontrola odpovedi
            String reply = in.readUTF();
            String[] fullReply = new String[6];
            fullReply = reply.split(" ", -1);

            if (fullReply[0].equals("AREC")){
                // TODO: Vyresit commence game

            }
            else if (fullReply[0].equals("DREC")){
                System.out.println("Server couldn't ready you. Reason: " + fullReply[1]);
            }
            else {
                System.out.println("Couldn't ready you, communication with server failed. Disconnecting.");
                this.socket = null;
                this.out = null;
                this.in = null;
            }
        }
        else{
            System.out.println("No server connection.");
        }
        */
    }

    public void startGame(){
        try{
            if (uiController instanceof GameSetupController){
                ((GameSetupController) uiController).gotoGameBoard();
            }
        } catch (IOException ex){
            sendMessageToController("Gameboard not found.");
            ex.printStackTrace();
        }
    }

    public void sendCustomCommand(String command) throws IOException {
        clientThread.sendCommand(command);
    }

    public int getMyId(){ return this.myId; }

    public Game getGame(){
        return this.game;
    }

    public SceneController getUiController() { return this.uiController; }

    public boolean isConnectedToServer() {
        if (this.socket == null){
            return false;
        }
        else {
            return true;
        }
    }
}
